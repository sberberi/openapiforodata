﻿using Microsoft.OData.Edm;
using Microsoft.OData.Edm.Csdl;
using Microsoft.OpenApi;
using Microsoft.OpenApi.Extensions;
using Microsoft.OpenApi.Models;
using Microsoft.OpenApi.OData;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace OpenApiForOdata2
{
    class Program
    {
        static void Main(string[] args)
        {
            // exe "C:\Users\sberberi\Desktop\testmetadata.xml" "outputSwagger"
            if (args.Length == 0)
            {
                Console.WriteLine("Please enter args : 1. A metadata file path \t 2.the filename of the output file(json).");
                return;
            }

            if (!File.Exists(args[0]))
            {
                Console.WriteLine("Invalid metadata file path");
                return;
            }

            if (args[1].IsEmptyWhitespaceOrNull())
            {
                Console.WriteLine("Invalid output filename");
                return;
            }

            string parentDirectory = Path.GetDirectoryName(Path.GetTempPath());

            // Argumenti i pare
            string csdlFilePath = args[0];
            //string csdlFilePath = @$"C:\Users\sberberi\Desktop\$metadata.xml";

            // Argumenti i dyte ==> Emri i file-t te output
            string outputFilename = args[1];

            IEdmModel model = GetEdmModelByCSDLFilePath(csdlFilePath);

            OpenApiDocument document = model.ConvertToOpenApi();

            string outputJSONString = document.SerializeAsJson(OpenApiSpecVersion.OpenApi3_0);

            string outputPath = Path.Combine(parentDirectory, $"{outputFilename}.json");

            File.WriteAllText(outputPath, outputJSONString);

            Console.WriteLine();
            Console.WriteLine($"Open-API for \"{args[0]}\" was created succesfully at \"{outputPath}\"!");
            Console.WriteLine();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }


        public static IEdmModel GetEdmModelByCSDLFilePath(string csdlFilePath)
        {
            string csdl = File.ReadAllText(csdlFilePath);
            IEdmModel model = CsdlReader.Parse(XElement.Parse(csdl).CreateReader());
            return model;
        }
    }

    public static class Extensions
    {
        /// <summary>
        /// Returs true if string is whitespace, null or empty
        /// </summary>
        /// <param name="str">string input</param>
        /// <returns>boolean value if string is empty or not</returns>
        public static bool IsEmptyWhitespaceOrNull(this string str)
                            => string.IsNullOrWhiteSpace(str) || string.Empty == str;

        /// <summary>
        /// Checks if filename is valid
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool IsValidFilename(this string fileName)
        {
            if (fileName.IsEmptyWhitespaceOrNull()) return false;

            string strTheseAreInvalidFileNameChars = new string(Path.GetInvalidFileNameChars());
            Regex regInvalidFileName = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");

            if (regInvalidFileName.IsMatch(fileName)) { return false; };

            return true;
        }

        public static bool IsLocalPath(this string path)
        {
            bool ret = true;
            try
            {
                ret = new Uri(path).IsFile;
            }
            catch
            {
                if (path.StartsWith("http://") ||
                    path.StartsWith(@"http:\\") ||
                    path.StartsWith("https://") ||
                    path.StartsWith(@"https:\\"))
                {
                    return false;
                }
            }

            return ret;
        }
    }
}
